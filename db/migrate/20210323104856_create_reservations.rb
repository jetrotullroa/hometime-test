class CreateReservations < ActiveRecord::Migration[6.1]
  def change
    create_table :reservations do |t|
      t.date :start_date
      t.date :end_date
      t.integer :nights, default: 0
      t.integer :guests, default: 0
      t.integer :adults, default: 0
      t.integer :children, default: 0
      t.integer :infants, default: 0
      t.integer :status, default: 0
      t.string :currency
      t.decimal :payout_price, precision: 10, scale: 2, default: 0.00
      t.decimal :security_price, precision: 10, scale: 2, default: 0.00
      t.decimal :total_price, precision: 10, scale: 2, default: 0.00
      t.references :guest, null: false, foreign_key: true

      t.timestamps
    end
  end
end
