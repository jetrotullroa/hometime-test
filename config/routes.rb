Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'page#index'

  namespace :api, defaults: { format: :json } do
    resources :reservations, only: %i[create]
  end
end

