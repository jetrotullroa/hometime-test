# hometime test API


### How to get started
#### WEB
You can visit https://hometime-test.herokuapp.com/api-docs/index.html and submit requests to API

#### Locally
For better experience use the latest Ruby(v3.0.0) and RoR(v6.1.3) versions.

Clone the repo
`git clone git@gitlab.com:jetrotullroa/hometime-test.git`

Run the following commands

$ `cd hometime-test`

$ `bundle install`

$ `rails db:create && rails db:migrate`

$ `rake rswag:specs:swaggerize`

$ `rails s`

visit http://localhost:3000/api-docs/index.html and start submitting requests to API

### JSON Format
Please make sure that you have a valid JSON format. You can validate it via https://jsonformatter.curiousconcept.com

Example 1
```json
{

   "reservation":{

      "start_date":"2020-03-12",

      "end_date":"2020-03-16",

      "expected_payout_amount":"3800.00",

      "guest_details":{

         "localized_description":"4 guests",

         "number_of_adults":2,

         "number_of_children":2,

         "number_of_infants":0

      },

      "guest_email":"wayne_woodbridge@bnb.com",

      "guest_first_name":"Wayne",

      "guest_id":1,

      "guest_last_name":"Woodbridge",

      "guest_phone_numbers":[

         "639123456789",

         "639123456789"

      ],

      "listing_security_price_accurate":"500.00",

      "host_currency":"AUD",

      "nights":4,

      "number_of_guests":4,

      "status_type":"accepted",

      "total_paid_amount_accurate":"4500.00"

   }

}
```

Example 2
```json
{
   "start_date":"2020-03-12",
   "end_date":"2020-03-16",
   "nights":4,
   "guests":4,
   "adults":2,
   "children":2,
   "infants":0,
   "status":"accepted",
   "guest":{
      "id":1,
      "first_name":"Wayne",
      "last_name":"Woodbridge",
      "phone":"639123456789",
      "email":"wayne_woodbridge@bnb.com"
   },
   "currency":"AUD",
   "payout_price":"3800.00",
   "security_price":"500",
   "total_price":"4500.00"
}
```