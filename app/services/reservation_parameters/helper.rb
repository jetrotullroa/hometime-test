module ReservationParameters
  class Helper

    def initialize(params = {})
      @guest_id = params.dig(:reservation, :guest_id) || params.dig(:guest, :id)
      @guest_email = params.dig(:reservation, :guest_email) || params.dig(:guest, :email)
      @guest_first_name = params.dig(:reservation, :guest_first_name) || params.dig(:guest, :first_name)
      @guest_last_name = params.dig(:reservation, :guest_last_name) || params.dig(:guest, :last_name)
      @guest_phone_numbers = params.dig(:reservation, :guest_phone_numbers) || params.dig(:guest, :phone)
      @start_date = params.dig(:reservation, :start_date) || params[:start_date]
      @end_date = params.dig(:reservation, :end_date) || params[:end_date]
      @nights = params.dig(:reservation, :nights) || params[:nights]
      @guests = params.dig(:reservation, :number_of_guests) || params[:guests]
      @adults = params.dig(:reservation, :guest_details, :number_of_adults) || params[:adults]
      @children = params.dig(:reservation, :guest_details, :number_of_children) || params[:children]
      @infants = params.dig(:reservation, :guest_details, :number_of_infants) || params[:infants]
      @status = params.dig(:reservation, :status_type) || params[:status]
      @currency = params.dig(:reservation, :host_currency) || params[:currency]
      @payout_price = params.dig(:reservation, :expected_payout_amount) || params[:payout_price]
      @security_price = params.dig(:reservation, :listing_security_price_accurate) || params[:security_price]
      @total_price = params.dig(:reservation, :total_paid_amount_accurate) || params[:total_price]
    end

    def guest
      guest = Guest.find_by(id: @guest_id)
      return find_or_create_guest unless guest
      
      find_and_update_guest(guest)
    end

    def reservation
      {
        start_date: @start_date,
        end_date: @end_date,
        nights: @nights,
        guests: @guests,
        adults: @adults,
        children: @children,
        infants: @infants,
        status: @status,
        currency: @currency,
        payout_price: @payout_price,
        security_price: @security_price,
        total_price: @total_price,
      }
    end

    private

    def find_or_create_guest
      guest = Guest.find_or_create_by(email: @guest_email)
      guest.update(
        first_name: @guest_first_name,
        last_name: @guest_last_name,
        phone: phone_numbers
      )
      guest
    end

    def find_and_update_guest(guest)
      guest.update!(
        email: @guest_email,
        first_name: @guest_first_name,
        last_name: @guest_last_name,
        phone: phone_numbers
      )
      guest
    end

    def phone_numbers
      return @guest_phone_numbers.join(", ") if @guest_phone_numbers.is_a?(Array)

      @guest_phone_numbers
    end
  end
end