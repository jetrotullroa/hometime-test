class ReservationSerializer < ActiveModel::Serializer
  attributes %i[id start_date end_date nights guests adults 
                children infants status currency payout_price 
                security_price total_price]

  belongs_to :guest
end
