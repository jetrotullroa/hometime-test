class Reservation < ApplicationRecord
  belongs_to :guest
  enum status: { pending: 0, accepted: 1, rejected: 2 }

end
