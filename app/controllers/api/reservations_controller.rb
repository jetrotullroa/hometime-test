module Api
  class ReservationsController < ApiController
    before_action :set_guest, only: %i[create]

    def create
      @reservation = @guest.reservations.build(reservation_params)
      return render json: { error: { status: 400,
                                     message: 'Invalid parameters' },
                            status: :bad_request } unless @reservation.save
      
      render json: @reservation, status: :created
    end

    private

    def reservation_params
      params.permit(%i[start_date end_date nights guests
                       adults children infants status currency
                       payout_price security_price total_price])
            .merge(reservation_body)
    end

    def reservation_body
      ReservationParameters::Helper.new(params).reservation
    end


    def set_guest
      @guest = ReservationParameters::Helper.new(params).guest
    end
  end
end
