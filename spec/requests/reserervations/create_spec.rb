require 'swagger_helper'

describe Api::ReservationsController, type: :request do
  path '/api/reservations' do
    post 'User able to create reservation' do
      tags 'Reservation'
      consumes 'application/json'
      produces 'application/json'

      parameter name: 'reservation', in: :body, schema: {
        type: :object,
        properties: {}
      }

      response '201', 'returns a newly created Reservation for a Guest' do
        run_test!
      end
    end
  end
end
