FactoryBot.define do
  factory :reservation do
    association :guest, factory: :guest

    start_date { (Time.zone.today + 2.days).strftime("%Y-%m-%d") }
    end_date { (Time.zone.today + 10.days).strftime("%Y-%m-%d") }
    nights { Faker::Number.between(from: 1, to: 20) }
    guests { 1 }
    adults { Faker::Number.between(from: 1, to: 2) }
    children { Faker::Number.between(from: 0, to: 4) }
    infants { Faker::Number.between(from: 0, to: 2) }
    status { 1 }
    currency { "AUD" }
    payout_price { Faker::Number.decimal(l_digits: 3, r_digits: 2) }
    security_price { Faker::Number.decimal(l_digits: 3, r_digits: 2) }
    total_price { Faker::Number.decimal(l_digits: 4, r_digits: 2) }

    after(:build) do |guest|
      guest.guests = guest.adults + guest.children + guest.infants
      guest.nights = (guest.end_date - guest.start_date).to_i
    end
  end
end
