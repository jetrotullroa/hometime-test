FactoryBot.define do
  factory :guest do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    sequence(:email) { |n| "guest#{n}@#{Faker::Internet.domain_name}" }
    phone { "63#{Faker::Number.number(digits: 10)}" }
  end
end
